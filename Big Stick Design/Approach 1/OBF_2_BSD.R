#############          O'Brien and Fleming, 2 Stages, Big Stick Design, applying the randomization procedure to all patients

#libraries
library(pracma)
library(cubature)
library(randomizeR)

#parameters
n = 60
K = 2
sigma_sqrt = 1
effect = 0
ergebnis.vector.alpha.2 = vector()
ergebnis.vector.alpha.1 = vector()
alpha_1 = vector()
alpha_2 = vector()
norep = 1000

eta_vector = c(0, 0.01,0.1,0.2,0.3,0.4,0.5)

p_vector = c(2, 5, 10)

Ergebnismatrix_1= matrix(rep(0,length(eta_vector)*(K+2)), nrow = length(eta_vector)) 
Ergebnismatrix_2 = matrix(rep(0,length(eta_vector)*(K+2)), nrow = length(eta_vector)) 
Ergebnismatrix_3 = matrix(rep(0,length(eta_vector)*(K+2)), nrow = length(eta_vector)) 
for (r in 1:length(p_vector))
{
  p = p_vector[r]
  
  for (m in 1:length(eta_vector))
  {
    
    
    eta = eta_vector[m]
    set.seed(161042)
    
    
    for (i in 1:norep)
    {
      randseq = bsdPar(n , mti = p , groups = c("0","1"))
      gensequence = genSeq(randseq,1)
      subsequence_1 = gensequence$M[1:(n/K)]
      
      no_T_1 = sum(subsequence_1)
      no_C_1 = n/K-no_T_1
      
      
      
      
      calc.nu = function(seq)
      {
        nu = rep(0,6)
        noT = 0
        noC = 0
        for (i in 1: length(seq))
        {
          if ( (noT == noC) & (seq[i] == 0))
          {noC = noC +1
          nu[2] = nu[2]+1
          } else if ( (noT > noC) & (seq[i] == 0) )
          {noC = noC +1
          nu[1] = nu[1]+1
          } else if ( (noT < noC) & (seq[i] == 0))
          {noC = noC +1
          nu[3] = nu[3]+1
          }else if ( (noT == noC) & (seq[i] == 1))
          {noT = noT +1
          nu[5] = nu[5]+1
          }else if ( (noT > noC) & (seq[i] == 1))
          {noT = noT +1
          nu[4] = nu[4]+1
          }else if ( (noT < noC) & (seq[i] == 1))
          {noT = noT +1
          nu[6] = nu[6]+1
          }
          
          
        }
        return(nu)
        
      }
      
      
      nu.stage1 = calc.nu(subsequence_1)
      nu.stage1
      
      
      ############################ Erwartungswert für Teststatistik ##############################
      vector.exp = c(1/no_C_1, 0, -1/no_C_1, -1/no_T_1, 0 , 1/no_T_1)
      factor = as.numeric((vector.exp %*% nu.stage1)[1,1])
      mu_1 = sqrt( 1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1)) ) * factor * eta 
      
      
      grobf_1 = 1.977 * sqrt(2) 
      grobf_2 = 1.977 
      ################################### Fehlerwahrscheinlichkeit in der 1.Stufe ################## 
      
      ftest_1 = function(x) 1/(sqrt(2*pi)) * exp( - (( x-mu_1)^2) / 2 ) 
      wert_1 = integrate(ftest_1, -grobf_1, grobf_1)
      alpha_1=  1-wert_1$value
      ergebnis.vector.alpha.1[i] = alpha_1
      
      
      
      ######################### Erwartungswert für Teststatistik in der 2. Stufe ########################## 
      no_T_2 = sum(gensequence@M)
      no_C_2 = n - no_T_2
      
      nu.stage2 = calc.nu(gensequence@M)
      vector.exp.secstage = c(1/no_C_2, 0, -1/no_C_2, -1/no_T_2, 0 , 1/no_T_2)
      factor2 = as.numeric((vector.exp.secstage %*% nu.stage2)[1,1])
      mu_2 = sqrt( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2)) ) * factor2 * eta 
      
      ######################## Kovarianz #######################
      ### Kovarianz von Z1 und Z2 
      
      cov = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) ) * ( sigma_sqrt/no_C_2 + sigma_sqrt/no_T_2)
      korr = cov/sqrt((sigma_sqrt * sigma_sqrt))
      
      
      
      ######################### Fehlerwahrscheinlichkeit der 2. Stufe ################################## 
      
      f <- function(x) (1/(2*pi* sqrt(1-korr^2) )) * exp( -1/(2*(1-korr^2))  * (  (x[1]-(mu_1))^2 +  (x[2]-(mu_2))^2 - 2* korr * (x[1]-mu_1)*(x[2] - mu_2)) )  
      wert_2 = pcubature(f, c(-grobf_1, -Inf), c(grobf_1, -grobf_2))$integral
      wert_3 = pcubature(f, c(-grobf_1, grobf_2), c(grobf_1, Inf))$integral
      alpha_2 = alpha_1 + wert_2 + wert_3
      
      ergebnis.vector.alpha.2[i] = alpha_2 
    }
    if (r == 1)
    {
      Ergebnismatrix_1[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_1[m,2] = mean(ergebnis.vector.alpha.2)
    }else if (r == 2)
    {
      Ergebnismatrix_2[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_2[m,2] = mean(ergebnis.vector.alpha.2)
    }else
    {
      Ergebnismatrix_3[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_3[m,2] = mean(ergebnis.vector.alpha.2)
    }
  }
}



for( i in 1:length(eta_vector))
{
  diff_vector_1 = Ergebnismatrix_1[i, 1:2] - Ergebnismatrix_1[1, 1:2]
  diff_vector_2 = Ergebnismatrix_2[i, 1:2] - Ergebnismatrix_2[1, 1:2]
  diff_vector_3 = Ergebnismatrix_3[i, 1:2] - Ergebnismatrix_3[1, 1:2]
  Ergebnismatrix_1[i, (K + 1):(K+2)] = diff_vector_1
  Ergebnismatrix_2[i, (K + 1):(K+2)] = diff_vector_2
  Ergebnismatrix_3[i, (K + 1):(K+2)] = diff_vector_3
}




n
norep
Ergebnis = cbind(round(Ergebnismatrix_1, digits = 3), round(Ergebnismatrix_2, digits = 3), round(Ergebnismatrix_3, digits = 3))
dimnames(Ergebnis) = list( c("0.00", "0.01", "0.1", "0.2", "0.3", "0.4", "0.5"), 
                           c("1", "2", "Diff 1", "Diff 2", "1", "2", "Diff 1", "Diff 2", "1", "2", "Diff 1", "Diff 2"))

library(xtable)

xtable(Ergebnis, digits = 3)

write.csv(Ergebnis, "OBF_2_BSD.csv")
