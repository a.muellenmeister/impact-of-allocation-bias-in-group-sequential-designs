#############       O'Brien and Fleming, 3 Stages, Chen's Design, applying the randomization procedure to all patients

#libraries
library(pracma)
library(cubature)
library(randomizeR)

#parameters
norep = 100000

################################################ n = 60 #########################################################################




n = 60
K = 3
sigma_sqrt = 1
effect = 0
ergebnis.vector.alpha.2 = vector()
ergebnis.vector.alpha.1 = vector()
ergebnis.vector.alpha.3 = vector()
alpha_1 = vector()
alpha_2 = vector()
alpha_3 = vector()


eta_vector = c(0, 0.01,0.1,0.2,0.3,0.4,0.5)

p_vector = c(3,5,10)


Ergebnismatrix_1= matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 
Ergebnismatrix_2 = matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 
Ergebnismatrix_3 = matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 


for (r in 1:length(p_vector))
{
  b = p_vector[r]
  
  for (m in 1:length(eta_vector))
  {
    
    
    eta = eta_vector[m]
    set.seed(161043)
    
    for (i in 1:norep)
    {
      randseq = chenPar(n , mti = b, p = 2/3 , groups = c("0","1"))
      gensequence = genSeq(randseq,1)
      subsequence_1 = gensequence$M[1:(n/K)]
      
      no_T_1 = sum(subsequence_1)
      no_C_1 = n/K-no_T_1
      
      
      
      
      calc.nu = function(seq)
      {
        nu = rep(0,6)
        noT = 0
        noC = 0
        for (i in 1: length(seq))
        {
          if ( (noT == noC) & (seq[i] == 0))
          {noC = noC +1
          nu[2] = nu[2]+1
          } else if ( (noT > noC) & (seq[i] == 0) )
          {noC = noC +1
          nu[1] = nu[1]+1
          } else if ( (noT < noC) & (seq[i] == 0))
          {noC = noC +1
          nu[3] = nu[3]+1
          }else if ( (noT == noC) & (seq[i] == 1))
          {noT = noT +1
          nu[5] = nu[5]+1
          }else if ( (noT > noC) & (seq[i] == 1))
          {noT = noT +1
          nu[4] = nu[4]+1
          }else if ( (noT < noC) & (seq[i] == 1))
          {noT = noT +1
          nu[6] = nu[6]+1
          }
          
          
        }
        return(nu)
        
      }
      
      
      nu.stage1 = calc.nu(subsequence_1)
      
      
      
      ############################ Erwartungswert für Teststatistik in der 1. Stufe  ##############################
      vector.exp = c(1/no_C_1, 0, -1/no_C_1, -1/no_T_1, 0 , 1/no_T_1)
      factor = as.numeric((vector.exp %*% nu.stage1)[1,1])
      mu_1 = sqrt( 1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1)) ) * factor * eta 
      
      
      
      
      ######################### Erwartungswert für Teststatistik in der 2. Stufe ########################## 
      Teilsequenz2 = gensequence$M[1:((n/K)*2)]
      no_T_2 = sum(Teilsequenz2)
      no_C_2 = 2*n/K - no_T_2
      
      nu.stage2 = calc.nu(Teilsequenz2)
      vector.exp.secstage = c(1/no_C_2, 0, -1/no_C_2, -1/no_T_2, 0 , 1/no_T_2)
      factor2 = as.numeric((vector.exp.secstage %*% nu.stage2)[1,1])
      mu_2 = sqrt( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2)) ) * factor2 * eta 
      
      
      
      
      
      ############################ Erwartungswert für Teststatistik in der 3. Stufe  ##############################
      no_T_3 = sum(gensequence@M)
      no_C_3 = n - no_T_3
      
      nu.stage3 = calc.nu(gensequence@M)
      vector.exp.thirdstage = c(1/no_C_3, 0, -1/no_C_3, -1/no_T_3, 0 , 1/no_T_3)
      factor3 = as.numeric((vector.exp.thirdstage %*% nu.stage3)[1,1])
      mu_3 = sqrt( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3)) ) * factor3 * eta 
      
      
      ########################### Kovarianzen von Z1 und Z2 ########################################### 
      cov_12 = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) ) * ( sigma_sqrt/no_C_2 + sigma_sqrt/no_T_2)
      korr_12 = cov_12/sqrt((sigma_sqrt * sigma_sqrt))
      
      ########################### Kovarianzen von Z1 und Z3 ########################################### 
      cov_13 = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3))) ) * ( sigma_sqrt/no_C_3 + sigma_sqrt/no_T_3)
      korr_13 = cov_13/sqrt((sigma_sqrt * sigma_sqrt))
      
      ########################### Kovarianzen von Z2 und Z3 ########################################### 
      cov_23 = sqrt(  (1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) * ( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3))) ) * ( sigma_sqrt/no_C_3 + sigma_sqrt/no_T_3)
      korr_23 = cov_23/sqrt((sigma_sqrt * sigma_sqrt))
      
      #### Kovarianzmatrix #############
      Sigma = matrix( c(1, cov_12, cov_13, cov_12, 1, cov_23, cov_13, cov_23, 1), nrow = 3)
      Sigma_inv = solve(Sigma)
      det_Sigma = det(Sigma)
      
      
      
      
      
      
      #### Fehlerwahrscheinlichkeiten #####
      
      
      grobf_1 = 2.004 * sqrt(3)
      grobf_2 = 2.004 * sqrt(3/2)
      grobf_3 = 2.004
      
      ######################### Fehlerwahrscheinlichkeit der 1. Stufe ################################## 
     
      ftest_1 = function(x) 1/(sqrt(2*pi)) * exp( - (( x-mu_1)^2) / 2 ) 
      wert_1 = integrate(ftest_1, -grobf_1, grobf_1)
      alpha_1=  1-wert_1$value
      ergebnis.vector.alpha.1[i] = alpha_1
      
      
      ######################### Fehlerwahrscheinlichkeit der 2. Stufe ################################## 
     
      f <- function(x) (1/(2*pi* sqrt(1-korr_12^2) )) * exp( -1/(2*(1-korr_12^2))  * (  (x[1]-(mu_1))^2 +  (x[2]-(mu_2))^2 - 2* korr_12 * (x[1]-mu_1)*(x[2] - mu_2)) )  
      wert_2 = pcubature(f, c(-grobf_1, -Inf), c(grobf_1, -grobf_2))$integral
      wert_3 = pcubature(f, c(-grobf_1, grobf_2), c(grobf_1, Inf))$integral
      alpha_2 = alpha_1 + wert_2 + wert_3
      
      ergebnis.vector.alpha.2[i] = alpha_2 
      
      ######################### Fehlerwahrscheinlichkeit der 3. Stufe ################################## 
      
      mu = c(mu_1, mu_2, mu_3)
      f <- function(x) (1/ sqrt((2*pi)^3 * det_Sigma)) * exp( -0.5 * t(c(x[1]-mu_1, x[2]-mu_2, x[3]-mu_3)) %*% Sigma_inv %*% c(x[1]-mu_1, x[2]-mu_2, x[3]-mu_3))  
      wert_4 = pcubature(f, c(-grobf_1, -grobf_2, -Inf), c(grobf_1, grobf_2, -grobf_3))$integral
      wert_5 = pcubature(f, c(-grobf_1, -grobf_2, grobf_3), c(grobf_1, grobf_2, Inf))$integral
      alpha_3 = alpha_2 + wert_4 + wert_5
      
      ergebnis.vector.alpha.3[i] = alpha_3 
      
      
      
      
    }
    if (r == 1)
    {
      Ergebnismatrix_1[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_1[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_1[m,3] = mean(ergebnis.vector.alpha.3)
    } else if (r == 2)
    {
      Ergebnismatrix_2[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_2[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_2[m,3] = mean(ergebnis.vector.alpha.3)
    }else
    {
      Ergebnismatrix_3[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_3[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_3[m,3] = mean(ergebnis.vector.alpha.3)
    }
  }
}




Ergebnis_1 = rbind(round(Ergebnismatrix_1, digits = 3), round(Ergebnismatrix_2, digits = 3), round(Ergebnismatrix_3, digits = 3))






################################################ n = 120 #########################################################################




n = 120
K = 3
sigma_sqrt = 1
effect = 0
ergebnis.vector.alpha.2 = vector()
ergebnis.vector.alpha.1 = vector()
ergebnis.vector.alpha.3 = vector()
alpha_1 = vector()
alpha_2 = vector()
alpha_3 = vector()


eta_vector = c(0, 0.01,0.1,0.2,0.3,0.4,0.5)

p_vector = c(3,5,10)


Ergebnismatrix_1= matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 
Ergebnismatrix_2 = matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 
Ergebnismatrix_3 = matrix(rep(0,length(eta_vector)*(K)), nrow = length(eta_vector)) 


for (r in 1:length(p_vector))
{
  b = p_vector[r]
  
  for (m in 1:length(eta_vector))
  {
    
    
    eta = eta_vector[m]
    set.seed(161043)
    
    for (i in 1:norep)
    {
      randseq = chenPar(n , mti = b, p = 2/3 , groups = c("0","1"))
      gensequence = genSeq(randseq,1)
      subsequence_1 = gensequence$M[1:(n/K)]
      
      no_T_1 = sum(subsequence_1)
      no_C_1 = n/K-no_T_1
      
      
      
      
      calc.nu = function(seq)
      {
        nu = rep(0,6)
        noT = 0
        noC = 0
        for (i in 1: length(seq))
        {
          if ( (noT == noC) & (seq[i] == 0))
          {noC = noC +1
          nu[2] = nu[2]+1
          } else if ( (noT > noC) & (seq[i] == 0) )
          {noC = noC +1
          nu[1] = nu[1]+1
          } else if ( (noT < noC) & (seq[i] == 0))
          {noC = noC +1
          nu[3] = nu[3]+1
          }else if ( (noT == noC) & (seq[i] == 1))
          {noT = noT +1
          nu[5] = nu[5]+1
          }else if ( (noT > noC) & (seq[i] == 1))
          {noT = noT +1
          nu[4] = nu[4]+1
          }else if ( (noT < noC) & (seq[i] == 1))
          {noT = noT +1
          nu[6] = nu[6]+1
          }
          
          
        }
        return(nu)
        
      }
      
      
      nu.stage1 = calc.nu(subsequence_1)
      
      
      
      ############################ Erwartungswert für Teststatistik in der 1. Stufe  ##############################
      vector.exp = c(1/no_C_1, 0, -1/no_C_1, -1/no_T_1, 0 , 1/no_T_1)
      factor = as.numeric((vector.exp %*% nu.stage1)[1,1])
      mu_1 = sqrt( 1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1)) ) * factor * eta 
      
      
      
      
      ######################### Erwartungswert für Teststatistik in der 2. Stufe ########################## 
      Teilsequenz2 = gensequence$M[1:((n/K)*2)]
      no_T_2 = sum(Teilsequenz2)
      no_C_2 = 2*n/K - no_T_2
      
      nu.stage2 = calc.nu(Teilsequenz2)
      vector.exp.secstage = c(1/no_C_2, 0, -1/no_C_2, -1/no_T_2, 0 , 1/no_T_2)
      factor2 = as.numeric((vector.exp.secstage %*% nu.stage2)[1,1])
      mu_2 = sqrt( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2)) ) * factor2 * eta 
      
      
      
      
      
      ############################ Erwartungswert für Teststatistik in der 3. Stufe  ##############################
      no_T_3 = sum(gensequence@M)
      no_C_3 = n - no_T_3
      
      nu.stage3 = calc.nu(gensequence@M)
      vector.exp.thirdstage = c(1/no_C_3, 0, -1/no_C_3, -1/no_T_3, 0 , 1/no_T_3)
      factor3 = as.numeric((vector.exp.thirdstage %*% nu.stage3)[1,1])
      mu_3 = sqrt( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3)) ) * factor3 * eta 
      
      
      ########################### Kovarianzen von Z1 und Z2 ########################################### 
      cov_12 = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) ) * ( sigma_sqrt/no_C_2 + sigma_sqrt/no_T_2)
      korr_12 = cov_12/sqrt((sigma_sqrt * sigma_sqrt))
      
      ########################### Kovarianzen von Z1 und Z3 ########################################### 
      cov_13 = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3))) ) * ( sigma_sqrt/no_C_3 + sigma_sqrt/no_T_3)
      korr_13 = cov_13/sqrt((sigma_sqrt * sigma_sqrt))
      
      ########################### Kovarianzen von Z2 und Z3 ########################################### 
      cov_23 = sqrt(  (1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) * ( 1/ (sigma_sqrt* (1/no_C_3 + 1/no_T_3))) ) * ( sigma_sqrt/no_C_3 + sigma_sqrt/no_T_3)
      korr_23 = cov_23/sqrt((sigma_sqrt * sigma_sqrt))
      
      #### Kovarianzmatrix #############
      Sigma = matrix( c(1, cov_12, cov_13, cov_12, 1, cov_23, cov_13, cov_23, 1), nrow = 3)
      Sigma_inv = solve(Sigma)
      det_Sigma = det(Sigma)
      
      
      
      
      
      
      #### Fehlerwahrscheinlichkeiten #####
      
      
      grobf_1 = 2.004 * sqrt(3)
      grobf_2 = 2.004 * sqrt(3/2)
      grobf_3 = 2.004
      
      ######################### Fehlerwahrscheinlichkeit der 1. Stufe ################################## 
    
      ftest_1 = function(x) 1/(sqrt(2*pi)) * exp( - (( x-mu_1)^2) / 2 ) 
      wert_1 = integrate(ftest_1, -grobf_1, grobf_1)
      alpha_1=  1-wert_1$value
      ergebnis.vector.alpha.1[i] = alpha_1
      
      
      ######################### Fehlerwahrscheinlichkeit der 2. Stufe ################################## 
  
      f <- function(x) (1/(2*pi* sqrt(1-korr_12^2) )) * exp( -1/(2*(1-korr_12^2))  * (  (x[1]-(mu_1))^2 +  (x[2]-(mu_2))^2 - 2* korr_12 * (x[1]-mu_1)*(x[2] - mu_2)) )  
      wert_2 = pcubature(f, c(-grobf_1, -Inf), c(grobf_1, -grobf_2))$integral
      wert_3 = pcubature(f, c(-grobf_1, grobf_2), c(grobf_1, Inf))$integral
      alpha_2 = alpha_1 + wert_2 + wert_3
      
      ergebnis.vector.alpha.2[i] = alpha_2 
      
      ######################### Fehlerwahrscheinlichkeit der 3. Stufe ################################## 

      mu = c(mu_1, mu_2, mu_3)
      f <- function(x) (1/ sqrt((2*pi)^3 * det_Sigma)) * exp( -0.5 * t(c(x[1]-mu_1, x[2]-mu_2, x[3]-mu_3)) %*% Sigma_inv %*% c(x[1]-mu_1, x[2]-mu_2, x[3]-mu_3))  
      wert_4 = pcubature(f, c(-grobf_1, -grobf_2, -Inf), c(grobf_1, grobf_2, -grobf_3))$integral
      wert_5 = pcubature(f, c(-grobf_1, -grobf_2, grobf_3), c(grobf_1, grobf_2, Inf))$integral
      alpha_3 = alpha_2 + wert_4 + wert_5
      
      ergebnis.vector.alpha.3[i] = alpha_3 
      
      
      
      
    }
    if (r == 1)
    {
      Ergebnismatrix_1[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_1[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_1[m,3] = mean(ergebnis.vector.alpha.3)
    } else if (r == 2)
    {
      Ergebnismatrix_2[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_2[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_2[m,3] = mean(ergebnis.vector.alpha.3)
    }else
    {
      Ergebnismatrix_3[m,1] =  mean(ergebnis.vector.alpha.1)
      Ergebnismatrix_3[m,2] = mean(ergebnis.vector.alpha.2)
      Ergebnismatrix_3[m,3] = mean(ergebnis.vector.alpha.3)
    }
  }
}




Ergebnis_2 = rbind(round(Ergebnismatrix_1, digits = 3), round(Ergebnismatrix_2, digits = 3), round(Ergebnismatrix_3, digits = 3))






################################################ Zusammenführen #########################################################################

Ergebnis = cbind(Ergebnis_1, Ergebnis_2)
norep
Ergebnis 
dimnames(Ergebnis) = list(rep( c(0, 0.01,0.1,0.2,0.3,0.4,0.5), 3))
library(xtable)
xtable(Ergebnis, digits = 3)
write.csv(Ergebnis, "OBF_3_Chen.csv")


