#############            Pocock, 2 Stages, Random Allocation Rule, applying the randomization procedure to each stage

#libraries
library(pracma)
library(cubature)
library(randomizeR)


#parameters
n = 60
K = 2
alpha = 0.05
sigma_sqrt = 1
effect = 0
norep = 100000
ergebnis.vector.alpha.2 = vector()
ergebnis.vector.alpha.1 = vector()
alpha_1 = vector()
alpha_2 = vector()

eta_vector = c(0, 0.01,0.1,0.2,0.3,0.4,0.5)

Ergebnismatrix_1= matrix(rep(0,length(eta_vector)*(K+2)), nrow = length(eta_vector)) 



for (m in 1:length(eta_vector))
{
  
  
  eta = eta_vector[m]
  set.seed(161942)
  
  grpoc = 2.178
  for (i in 1:norep)
  {
    randseq = rarPar(n/K , K=2, groups = c("0","1"))
    gensequence = genSeq(randseq,1)
    subsequence_1 = gensequence$M
    
    no_T_1 = sum(subsequence_1)
    no_C_1 = n/K-no_T_1
    
    
    
    
    calc.nu = function(seq)
    {
      nu = rep(0,6)
      noT = 0
      noC = 0
      for (i in 1: length(seq))
      {
        
        if ( (noT == noC) & (seq[i] == 0))
        {noC = noC +1
        nu[2] = nu[2]+1
        } else if ( (noT > noC) & (seq[i] == 0) )
        {noC = noC +1
        nu[1] = nu[1]+1
        } else if ( (noT < noC) & (seq[i] == 0))
        {noC = noC +1
        nu[3] = nu[3]+1
        }else if ( (noT == noC) & (seq[i] == 1))
        {noT = noT +1
        nu[5] = nu[5]+1
        }else if ( (noT > noC) & (seq[i] == 1))
        {noT = noT +1
        nu[4] = nu[4]+1
        }else if ( (noT < noC) & (seq[i] == 1))
        {noT = noT +1
        nu[6] = nu[6]+1
        }
        
        
      }
      return(nu)
      
    }
    
    
    nu.stage1 = calc.nu(subsequence_1)
    nu.stage1
    
    
    ############################ Erwartungswert für Teststatistik ##############################
    vector.exp = c(1/no_C_1, 0, -1/no_C_1, -1/no_T_1, 0 , 1/no_T_1)
    factor = as.numeric((vector.exp %*% nu.stage1)[1,1])
    mu_1 = sqrt( 1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1)) ) * factor * eta 
    
    
    
    ################################### Fehlerwahrscheinlichkeit in der 1.Stufe ################## 
    #library(pracma)
    ftest_1 = function(x) 1/(sqrt(2*pi)) * exp( - (( x-mu_1)^2) / 2 ) 
    wert_1 = integrate(ftest_1, -2.178, 2.178)
    alpha_1=  1-wert_1$value
    ergebnis.vector.alpha.1[i] = alpha_1
    
    
    
    ######################### Erwartungswert für Teststatistik in der 2. Stufe ########################## 
    randseq_2 = rarPar(n/K , K=2, groups = c("0","1"))
    gensequence_2 = genSeq(randseq_2,1)
    Teilsequenz2 = c(gensequence$M, gensequence_2$M)
    no_T_2 = sum(Teilsequenz2)
    no_C_2 = n - no_T_2
    
    
    nu.stage2 = calc.nu(Teilsequenz2)
    vector.exp.secstage = c(1/no_C_2, 0, -1/no_C_2, -1/no_T_2, 0 , 1/no_T_2)
    factor2 = as.numeric((vector.exp.secstage %*% nu.stage2)[1,1])
    mu_2 = sqrt( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2)) ) * factor2 * eta 
    
    ######################## Kovarianz #######################
    ### Kovarianz von Z1 und Z2 
    
    cov = sqrt(  (1/ (sigma_sqrt* (1/no_C_1 + 1/no_T_1))) * ( 1/ (sigma_sqrt* (1/no_C_2 + 1/no_T_2))) ) * ( sigma_sqrt/no_C_2 + sigma_sqrt/no_T_2)
    korr = cov/sqrt((sigma_sqrt * sigma_sqrt))
    
    
    
    ######################### Fehlerwahrscheinlichkeit der 2. Stufe ################################## 
    #library(cubature)
    f <- function(x) (1/(2*pi* sqrt(1-korr^2) )) * exp( -1/(2*(1-korr^2))  * (  (x[1]-(mu_1))^2 +  (x[2]-(mu_2))^2 - 2* korr * (x[1]-mu_1)*(x[2] - mu_2)) )  
    wert_2 = pcubature(f, c(-2.178, -Inf), c(2.178, -2.178))$integral
    wert_3 = pcubature(f, c(-2.178, 2.178), c(2.178, Inf))$integral
    alpha_2 = alpha_1 + wert_2 + wert_3
    
    ergebnis.vector.alpha.2[i] = alpha_2 
  }
  Ergebnismatrix_1[m,1] = mean(ergebnis.vector.alpha.1)
  Ergebnismatrix_1[m,2] = mean(ergebnis.vector.alpha.2)
  
}




for( i in 1:length(eta_vector))
{
  diff_vector_1 = Ergebnismatrix_1[i, 1:2] - Ergebnismatrix_1[1, 1:2]
  Ergebnismatrix_1[i, (K + 1):(K+2)] = diff_vector_1
  
  
}

print("RAR Pocock Approach 2")
print(n)
print(norep)
Ergebnis = round(Ergebnismatrix_1, digits = 3)
dimnames(Ergebnis) = list( c("0.00", "0.01", "0.1", "0.2", "0.3", "0.4", "0.5"), 
                           c("1", "2", "Diff 1", "Diff 2"))
Ergebnis

library(xtable)

xtable(Ergebnis, digits = 3)


write.csv(Ergebnis, "Pocock_2_RAR.app2.csv")

